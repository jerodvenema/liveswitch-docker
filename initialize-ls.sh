# add curl and jq for making web requests to the LS api
apk --no-cache add curl
apk --no-cache add jq

# failures are critical?
#set -e
if [ -z $LIVESWITCH_USERNAME ]
then
    echo -e "\nLIVESWITCH_USERNAME not set, please check your environment variables\n"
    exit 1
fi

if [ -z $LIVESWITCH_PASSWORD ]
then
    echo -e "\nLIVESWITCH_PASSWORD not set, please check your environment variables\n"
    exit 1
fi

if [ -z $LIVESWITCH_LICENSE_KEY ]
then
    echo -e "\nLIVESWITCH_LICENSE_KEY not set, please check your environment variables\n"
    exit 1
fi

if [ -z $LIVESWITCH_APP_ID ]
then
    echo -e "\nLIVESWITCH_APP_ID not set, please check your environment variables\n"
    exit 1
fi

if [ -z $LIVESWITCH_SHARED_SECRET ]
then
    echo -e "\nLIVESWITCH_SHARED_SECRET not set, please check your environment variables\n"
    exit 1
fi

echo -e "\nInitializing..."
INITIALIZED="Initialized"

# check for existing initialization
SETUP_RESULT=$(curl -s -X GET "http://$LIVESWITCH_HOST:9090/admin/api/v2/site/Isinitialized?$format=json" \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \ | jq -r '.value')

echo -e "\nSetup result: $INITIALIZED"

if [ "$SETUP_RESULT" = "$INITIALIZED" ]
then
    echo -e "\nConfiguration already applied\n"
    exit 0
fi

# initialize the site;
echo -e "\nInitializing the server...\n"
curl -s -X POST http://$LIVESWITCH_HOST:9090/admin/api/v2/site/Initialize \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-d "{ username: \"$LIVESWITCH_USERNAME\", password:\"$LIVESWITCH_PASSWORD\", licenseKey: \"$LIVESWITCH_LICENSE_KEY\" }"


echo -e "\nGetting API token...\n"
# get an access token from the server so we can configure a few more things
LIVESWITCH_ACCESS_TOKEN=$(curl -s -X POST "http://$LIVESWITCH_HOST:9090/admin/connect/token" \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  --data-raw "client_id=dashboard&grant_type=password&username=$LIVESWITCH_USERNAME&password=$LIVESWITCH_PASSWORD" | jq -r '.access_token')

if [ -z "$LIVESWITCH_ACCESS_TOKEN" ]
then
    echo -e "\nAccess token not found\n"
    exit 1
fi

echo -e "\nConfiguring using $LIVESWITCH_ACCESS_TOKEN...\n"


echo -e "\nLocking down...\n"

# lock down the legacy api
curl -s -X PATCH \
http://$LIVESWITCH_HOST:9090/admin/api/v2/site \
-H 'Content-Type: application/json' \
-H 'Accept: application/json' \
-H "Authorization: bearer $LIVESWITCH_ACCESS_TOKEN"  \
-d '{"allowAnonymousLegacyApi":false}'

echo -e "\nCreating application...\n"

# create an application
curl -s -X POST \
http://$LIVESWITCH_HOST:9090/admin/api/v2/applicationConfig \
  -H 'Accept: application/json' \
  -H "Authorization: bearer $LIVESWITCH_ACCESS_TOKEN" \
  -H 'Content-Type: application/json' \
  --data-binary "{\"applicationId\":\"$LIVESWITCH_APP_ID\",\"sharedSecret\":\"$LIVESWITCH_SHARED_SECRET\"}" \
  --compressed


echo -e "\nGetting default deployment config..."

# get the default deployment config
LIVESWITCH_DEFAULT_DEPLOYMENT=$(curl -s -X GET \
http://$LIVESWITCH_HOST:9090/admin/api/v2/deploymentconfig \
  -H 'Accept: application/json' \
  -H "Authorization: bearer $LIVESWITCH_ACCESS_TOKEN" \
  -H 'Content-Type: application/json' | jq -r '.value[0] .id')

if [ -z "$LIVESWITCH_DEFAULT_DEPLOYMENT" ]
then
    echo -e "\nDeployment id not found\n"
    exit 1
fi


echo -e "\nUpdating gateway url..."

# and update it to have the URL according to the docker setup
curl -s -X PATCH \
"http://$LIVESWITCH_HOST:9090/admin/api/v2/deploymentconfig($LIVESWITCH_DEFAULT_DEPLOYMENT)" \
  -H 'Accept: application/json' \
  -H "Authorization: bearer $LIVESWITCH_ACCESS_TOKEN" \
  -H 'Content-Type: application/json' \
  --data-binary "{\"serviceBaseUrl\":\"http://$LIVESWITCH_HOST:8080\"}" \
  --compressed

echo -e "\nConfiguration complete.\n"