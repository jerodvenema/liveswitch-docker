# WARNING
This docker compose file is ONLY suited for getting started in a development environment. It is NOT intended for production use! You must handle failover, redundancy, backups, recording, autoscaling, etc, in your production infrastructure and this script does NOT do so. 

# Overview
This is pretty straightforward. It's just a docker-compose file for LiveSwitch. It's not set up for mass scale or health checks yet, just the basic create

# Prerequisites

docker and docker-compose

# Usage

The usage of this is about as straightforward as the overview.

1. Check out this repo
2. Run `configure.sh` to set up your environment variables (or copy liveswitch.sample.env to liveswitch.env, and update the environment variables in there according to what you want. Comments inline explain the variables.)
3. Run `docker-compose up`

That's it. Everything should be running locally. http://localhost:9090/admin to get to the admin page, http://localhost:8080/sync for signalling 

If you hit any issues on step 2, the configure.sh script does not do anything amazing. If it doesn't work, copy liveswitch.sample.env to liveswitch.env and edit the file manually with any text editor. Then continue with step 3.

# Note

The configuration script in this repository is just a helper to walk through setting some environment stuff up. It is only tested on a Mac, but should work fine on Linux, or the Linux subsystem on Windows.

# What this does

This compose file creates a LiveSwitch gateway and media server, as well as a redis instance, configures everything according to the variables defined in liveswitch.env, and connects everything together. It is a quick and dirty way of getting up and running with a local instance of LiveSwitch for development purposes.

# Debugging

We use a "setup" docker instance + the `initialize-ls.sh` script to create/setup the gateway configuration, so that's where the bulk of the work lives. If you're having issues, start there.

# Testing

This project validates the LiveSwitch setup using Cypress. To use Cypress, install it:

```bash
npm install cypress --save-dev
```

Then, in the root of the project, to use Cypress via the UI:

```bash
npm run cypress:open
```

...or from the command line:

```bash
npm run cypress:run
```

# CI

The CI for this project is pretty straightforward. It uses a docker Alpine image, installs docker compose on that docker isntance (yo dawg, I heard you like docker...) and then runs docker compose to fire up the instance. When configuration is completed, it runs the Cypress test suite and exits. 

*This CI relies on two environment variables!* 

In bitbucket pipelines we do this via repository settings->pipelines->repository variables.

`LIVESWITCH_LICENSE_KEY`: the license key for LiveSwitch
`CYPRESS_CACHE_FOLDER`: the cache folder for Cypress, typically ./cypress works fine
