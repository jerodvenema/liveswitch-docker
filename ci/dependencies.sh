#!/usr/bin/env sh

set -eu

#cat > /etc/apk/repositories << EOF; $(echo)
#http://dl-cdn.alpinelinux.org/alpine/v$(cat /etc/alpine-release | cut -d'.' -f1,2)/main
#http://dl-cdn.alpinelinux.org/alpine/v$(cat /etc/alpine-release | cut -d'.' -f1,2)/community
#EOF

#apk update

#apk add docker-compose
#docker-compose -v

#apk add --update npm

apt-get install --no-install-recommends -y docker docker-compose

npm install
npm install cypress --save-dev