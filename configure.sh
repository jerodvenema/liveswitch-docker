while [ -z $LICENSE_KEY ]
do
  read -p "Enter your LiveSwitch license key: " LICENSE_KEY
  if [ -z $LICENSE_KEY ]
  then
    echo "License key is required"
  fi
done

USERNAME_PASSED=0
while [ $USERNAME_PASSED -ne 1 ]
do
  read -p "Enter your admin console username: " USERNAME

  USERNAME_PASSED=1
  if [ -z "$USERNAME" ]
  then
    echo "Username is required"
    USERNAME_PASSED=0
  fi

  LEN=${#USERNAME}
  if [ $LEN -lt 7 ]
  then
    echo "Username must be at least 7 characters, yours is $LEN"
    USERNAME_PASSED=0
  fi
done

PASSWORD_PASSED=0
while [ $PASSWORD_PASSED -ne 1 ]
do
  read -s -p "Enter your admin console password: " PASSWORD

  PASSWORD_PASSED=1
  if [ -z "$PASSWORD" ]
  then
    echo "Password is required"
    PASSWORD_PASSED=0
  fi

  LEN=${#PASSWORD}
  if [ $LEN -lt 7 ]
  then
    echo "Password must be at least 7 characters, yours is $LEN"
    PASSWORD_PASSED=0
  fi
done

echo ""

while [ -z "$APPID" ]
do
  read -p "Enter your application id (this is the name of your app, like \"My App\"): " APPID

  if [ -z "$APPID" ]
  then
    echo "Application id is required"
  fi
done

SHARED_SECRET=$(openssl rand -base64 32 | tr -d "=+/")

cp -i ./liveswitch.sample.env ./liveswitch.env
sed -i '' -e "s/LIVESWITCH_USERNAME=/LIVESWITCH_USERNAME=$USERNAME/" ./liveswitch.env
sed -i '' -e "s/LIVESWITCH_PASSWORD=/LIVESWITCH_PASSWORD=$PASSWORD/" ./liveswitch.env
sed -i '' -e "s~LIVESWITCH_LICENSE_KEY=~LIVESWITCH_LICENSE_KEY=$LICENSE_KEY~" ./liveswitch.env
sed -i '' -e "s/LIVESWITCH_APP_ID=/LIVESWITCH_APP_ID=$APPID/" ./liveswitch.env
sed -i '' -e "s/LIVESWITCH_SHARED_SECRET=/LIVESWITCH_SHARED_SECRET=$SHARED_SECRET/" ./liveswitch.env

echo ""
echo "Configuration complete. Your shared secret is: $SHARED_SECRET. Please write this down somewhere safe."